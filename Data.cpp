#include "Data.h"

 Data::Data(TTree* tree) : m_tree(tree) {
	 m_tree->SetBranchAddress("weight_mc", &weight_mc);
	 m_tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
	 m_tree->SetBranchAddress("weight_pileup", &weight_pileup);
	 m_tree->SetBranchAddress("weight_leptonSF", &weight_leptonSF);
	 m_tree->SetBranchAddress("weight_photonSF", &weight_photonSF);
	 m_tree->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77);
	 m_tree->SetBranchAddress("weight_jvt", &weight_jvt);
	 m_tree->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso);
	 m_tree->SetBranchAddress("mu", &mu);
	 m_tree->SetBranchAddress("el_pt", &el_pt);
	 m_tree->SetBranchAddress("el_eta", &el_eta);
	 m_tree->SetBranchAddress("mu_pt", &mu_pt);
	 m_tree->SetBranchAddress("mu_eta", &mu_eta);
	 m_tree->SetBranchAddress("el_phi", &el_phi);
	 m_tree->SetBranchAddress("ph_pt", &ph_pt);
	 m_tree->SetBranchAddress("ph_eta", &ph_eta);
	 m_tree->SetBranchAddress("ph_iso", &ph_iso);
	 m_tree->SetBranchAddress("ejets_2015", &ejets_2015);
	 m_tree->SetBranchAddress("ejets_2016", &ejets_2016);
	 m_tree->SetBranchAddress("ejets_2017", &ejets_2017);
	 m_tree->SetBranchAddress("ejets_2018", &ejets_2018);
	 m_tree->SetBranchAddress("mujets_2015", &mujets_2015);
	 m_tree->SetBranchAddress("mujets_2016", &mujets_2016);
	 m_tree->SetBranchAddress("mujets_2017", &mujets_2017);
	 m_tree->SetBranchAddress("mujets_2018", &mujets_2018);
	 m_tree->SetBranchAddress("event_njets", &event_njets);
	 m_tree->SetBranchAddress("event_nbjets77", &event_nbjets77);
	 m_tree->SetBranchAddress("ph_good_index", &ph_good_index);
	 m_tree->SetBranchAddress("ph_true_category", &ph_true_category);
	 m_tree->SetBranchAddress("ph_conversion_type", &ph_conversion_type);
	 m_tree->SetBranchAddress("ph_mlph_closest", &ph_mlph_closest);
	 m_tree->SetBranchAddress("ph_mllph", &ph_mllph);
	 m_tree->SetBranchAddress("ph_iso_FCT", &ph_iso_FCT);
	 m_tree->SetBranchAddress("ph_id_tight", &ph_id_tight);
	 m_tree->SetBranchAddress("event_PV_z", &event_PV_z);
	 m_tree->SetBranchAddress("ph_z", &ph_z);
	 m_tree->SetBranchAddress("ph_dz", &ph_dz);
	 m_tree->SetBranchAddress("ph_dz_ratio", &ph_dz_ratio);
	 m_tree->SetBranchAddress("event_lumi", &event_lumi);
	 m_tree->SetBranchAddress("event_norm", &event_norm);

  

}
