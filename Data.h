#ifndef DATA_H
#define DATA_H

#include "TTree.h"
#include <vector>

class Data{
 public: 
  /**
  * @brief Construct a new Data object
  * 
  * @param tree - pointer to the TTree (or TChain) class
  */
  Data(TTree* tree);

  /**
  * @breif TTree variables
  */
  Float_t         weight_mc;
  UInt_t          mcChannelNumber;
  Float_t         weight_pileup;
  Float_t         weight_leptonSF;
  Float_t         weight_photonSF;
  Float_t         weight_bTagSF_DL1r_77;
  Float_t         weight_jvt;
  Float_t         weight_photonSF_effIso;
  Float_t         mu;
  std::vector<float>   *el_pt = 0;
  std::vector<float>   *el_eta = 0;
  std::vector<float>   *mu_pt = 0;
  std::vector<float>   *mu_eta = 0;
  std::vector<float>   *el_phi = 0;
  std::vector<float>   *ph_pt = 0;
  std::vector<float>   *ph_eta = 0;
  std::vector<float>   *ph_iso = 0;
  Int_t           ejets_2015;
  Int_t           ejets_2016;
  Int_t           ejets_2017;
  Int_t           ejets_2018;
  Int_t           mujets_2015;
  Int_t           mujets_2016;
  Int_t           mujets_2017;
  Int_t           mujets_2018;
  Int_t           event_njets;
  Int_t           event_nbjets77;
  std::vector<int>     *ph_good_index = 0;
  std::vector<int>     *ph_true_category = 0;
  std::vector<int>     *ph_conversion_type = 0;
  std::vector<float>   *ph_mlph_closest = 0;
  std::vector<float>   *ph_mllph = 0;
  std::vector<char>    *ph_iso_FCT = 0;
	std::vector<char>    *ph_id_tight = 0;
  Float_t         event_PV_z;
  std::vector<float>   *ph_z = 0;
  std::vector<float>   *ph_dz = 0;
  std::vector<float>   *ph_dz_ratio = 0;
  Float_t         event_lumi;
  Float_t         event_norm;


 protected:

  /**
  * @brief pointer to the TTree (or TChain) class
  */
  TTree* m_tree = 0;

};

#endif
