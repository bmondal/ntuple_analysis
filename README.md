This project is created for analyzing the ntuple.

**Descriptions:**

- Data class contains the structure of the Tree 

- EventLoop class handles the looping over each event 

- Algorithm - Here we write the code for calculating different things

- Cut class helps to use selections without hardcoding it inside the algorithm

- Algorithm.py, EventLoop.py are python wrappers around the c++ class.

- Samples.py contains the information about the samples

- runMe.py this is the main executable.

**Steps:**
The best directory sturcture should be like this:
`build`, `run`, `source`.

```bash
$ mkdir build run
$ git clone
$ mv Ntuple_Analysis source  # renaming the cloned repo to "source"
```

1. **Compiling**

```bash
cd build
source ../source/setup.sh
cmake ../source/
make
```


2. **Running:** from the `run` directory `python ../source/runMe.py` will run over the input samples and create output root file containing
histograms objects with information of z position of primary vertex and of photon. 
Input samples path can be chaged in the Samples.py
3. **Plotting:** `python ../source/PlotMe.py` can be used to make control plots and other plots with the output root files.

