from ROOT import EventLoop as EventLoopCpp


class EventLoop(object):
  """ Wrapper around the c++ class event loop
  """

  def __init__(self, name):
    # set the event loop name
    self.name = name

    # now we can create instance of the c++ class EventLoop
    self.eventLoop = EventLoopCpp()

    # set the tree name attribute
    self.eventLoop.treeName = "nominal"

    # keep the list of python Algorithm class instances
    self.algs = []

    # sample meta-data
    self.isMC = False
    # if it is MC16e, apply different event weight
    self.isMC16e_sample = False
    # same for tty decay samples
    self.isMC_decay_sample = False

  def addAlgorithm(self, algorithm):
    """ add an algorithm into this event loop
  """
    algorithm.setSumw2()
    self.algs += [algorithm]
    self.eventLoop.algorithms.push_back(algorithm.alg)

  def addAlgorithms(self, algorithms):
    """ add multiple algorithms into this event loop
  """
    for alg in algorithms:
      self.addAlgorithm(alg)

  def execute(self):
    """ initialize and execute the event loop
  """
    # load sumOfWeights and initialize algorithms
    if self.isMC:
      self.setSampleWeight()

    self.eventLoop.initialize()

    self.eventLoop.execute()

  def save(self):
    """ save histograms from all algorithms
  """
    for alg in self.algs:
      alg.save(self.name)

  def setSampleWeight(self):
    """ Set additional factor in the sample weight for mc16a, mc16d, mc16e
    """
    if self.isMC:
      for alg in self.algs:
        alg.alg.p_isMC = True
        if self.isMC16e_sample:
          alg.alg.p_isMC16eSample = True
        if self.isMC_decay_sample:
          alg.alg.p_isMCDecaySample = True
