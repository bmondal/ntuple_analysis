
#ifndef CUT_H
#define CUT_H

class Cut {
 public:
  /**
  * @brief Construct a new uninitialized cut
  */
  Cut();

  /**
  * @brief Sets the cut value
  */
  void set(double value);

  /**
  * @brief Returns true if value of this cut was set
  */
  bool enabled();
 
  /**
  * @brief Overloaded comparison operators
  */
  bool operator>(const double& rhs);
  bool operator<(const double& rhs);
  bool operator>=(const double& rhs);
  bool operator<=(const double& rhs);
  bool operator==(const double& rhs);
  bool operator!=(const double& rhs);

 protected:
  /**
  * @brief Indicates whether cut was initialized
  */
  bool m_enabled;

  /**
  * @brief The cut value
  */
  double m_value;

};

#endif
