#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "TString.h"
#include "TH1D.h"
#include "Data.h"
#include "Cut.h"

class Algorithm {
 public: 
  /**
  * @brief Construct a new Event Loop object
  */
  Algorithm();

  /**
   * @brief destructor
   */
  ~Algorithm();

  /**
  * @brief Initialize the algorithm
  * @param data - pointer to the data-access class instance
  */
  void initialize(Data* data);

  /**
  * @brief Execute. Here the stuff happens
  */
  void execute();

  /**
  * @brief Is the sample data or MC
  */
  bool p_isMC = false;

  /**
  * @brief which campaign MCa, MCd, MCe
  */
  bool p_isMC16eSample = false;
  bool p_isMCDecaySample= false;

  /**
   * @brief event weight
   */
  double p_sampleWeight = 1.;

  /**
  * @brief Pointer to the histogram class instance defined as public attribute
  */
  TH1D* h_ph_pt = 0; // must be initialized to 0
	TH1D* h_ph_eta = 0;
	TH1D* h_delta_z_pv = 0;
	TH1D* h_z_pv = 0;
	TH1D* h_z_ph = 0;

	/**
	 * @brief Selection cuts implemented using the "Cut" class
	 */
	bool is_mu_cut = false;
	Cut cut_mu_min;
	Cut cut_mu_max;

	/**
	 * @brief whether the photon is h-fake photon
	 */
	bool isPhotonHfake = false;

 protected:

	/**
	 * @brief Apply the selection
	 *
	 * @return true when event passed the selection.
	 */
	bool passedSelection();

	/**
	 * @brief Evaluate the event weight
	 */
	void calculateWeight();

	/**
	 * @brief Fill the histograms
	 */
	void fillPlots();

  /**
  * @brief Instance of the data-access class
  */
  Data* m_data = 0;

  /**
   * @brief Total event weight when filling histograms
   */
  double m_eventWeight;

};

#endif
