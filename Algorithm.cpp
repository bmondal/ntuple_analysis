#include "Algorithm.h"

Algorithm::Algorithm() {
}

void Algorithm::initialize(Data* data) {
 m_data = data;
}

void Algorithm::execute() {

// apply selection. Exit if didn't pass
 if(!passedSelection()) return;

  // check if the event contain h-fake photon then apply additional scale factor
  if(m_data->ph_true_category->at(0) == 10){
    isPhotonHfake = true;
  }

  // calculate the event weight
 calculateWeight();

 // fill the plots
 fillPlots();
}

bool Algorithm::passedSelection() {

	if(!(m_data->ph_good_index->size() == 1)) return false;
	if(m_data->ejets_2015 || m_data->ejets_2016 || m_data->ejets_2017 || m_data->ejets_2018 ){
		if(m_data->el_eta->size() != 1) return false;
		if(!(m_data->ph_mlph_closest->at(m_data->ph_good_index->at(0)) > 95e3 || m_data->ph_mlph_closest->at(m_data->ph_good_index->at(0)) < 85e3) ) return false;
		if(!((fabs(m_data->el_eta->at(0))<1.37 || fabs(m_data->el_eta->at(0))>1.52) && fabs(m_data->el_eta->at(0))<2.47)) return false;
	} 
	if(!(m_data->event_njets >= 4)) return false;
	if(!(m_data->event_nbjets77 >= 1)) return false;
	if(!(m_data->ph_id_tight->at(m_data->ph_good_index->at(0)) ) ) return false;
	if(!(m_data->ph_iso_FCT->at(m_data->ph_good_index->at(0)) ) ) return false;

	if(m_data->mujets_2015 || m_data->mujets_2016 || m_data->mujets_2017 || m_data->mujets_2018){
		if(m_data->mu_eta->size() != 1) return false;
		if(!(fabs(m_data->mu_eta->at(0))<2.5)) return false;
	}

	// select events with mu cut
	if(is_mu_cut){
    if(!(cut_mu_min.enabled() && cut_mu_max.enabled() && cut_mu_min < (m_data->mu) && cut_mu_max > (m_data->mu)) ) return false;
  }
	else{
	  return true;
	}

 // passed all the cuts
 return true;
}

void Algorithm::fillPlots() {
 // here we fill the histograms. We protect the code against the empty pointers.
 if(h_ph_pt)
 {
	 for(float i : *m_data->ph_pt){
		 h_ph_pt->Fill( i/1000.0, m_eventWeight);
	 }
 }

 if(h_ph_eta)
 {
	 for(float i : *m_data->ph_eta){
		 h_ph_eta->Fill( i, m_eventWeight);
	 }
 }

 if(h_delta_z_pv){
	 double z_variable = (m_data->event_PV_z - m_data->ph_z->at(0) )/(m_data->ph_dz->at(0));
	 h_delta_z_pv->Fill(z_variable, m_eventWeight);
 }

 if(h_z_pv){
	 h_z_pv->Fill(m_data->event_PV_z, m_eventWeight);
 }

 if(h_z_ph){
	 h_z_ph->Fill(m_data->ph_z->at(0), m_eventWeight);
 }

}

void Algorithm::calculateWeight(){
	// apply the sample weight
	m_eventWeight = p_sampleWeight;

  // apply the MC weight, if requested
  if(p_isMC){
    if(p_isMC16eSample) {
      m_eventWeight = 0;
      m_eventWeight = m_data->weight_mc*m_data->weight_pileup*m_data->weight_leptonSF*m_data->weight_photonSF*
                       m_data->weight_jvt*m_data->event_lumi*m_data->weight_photonSF_effIso*m_data->weight_bTagSF_DL1r_77*0.00000010248;
    }
    else if(p_isMCDecaySample)
    {
      m_eventWeight = 0;
      m_eventWeight = m_data->weight_mc*m_data->weight_pileup*m_data->weight_leptonSF*m_data->weight_photonSF*
              m_data->weight_jvt*m_data->event_norm*m_data->event_lumi*m_data->weight_photonSF_effIso*m_data->weight_bTagSF_DL1r_77*1.3*1.1584;
    }
    else{
      m_eventWeight = 0;
      m_eventWeight = m_data->weight_mc*m_data->weight_pileup*m_data->weight_leptonSF*m_data->weight_photonSF*
              m_data->weight_jvt*m_data->event_norm*m_data->event_lumi*m_data->weight_photonSF_effIso*m_data->weight_bTagSF_DL1r_77; // efake and hadron fake SF not included
    }

    if(isPhotonHfake){
      m_eventWeight *= 1.6; // if the photon is hfake then multiply with 1.6
      isPhotonHfake = false; // turn off the photon hfake flag
    }

  }

}
