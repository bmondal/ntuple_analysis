#include "EventLoop.h"
#include <iostream>
#include <stdexcept>

EventLoop::EventLoop() {
 // nothing to do here
}

void EventLoop::initialize() {
 // create an instance of the TChain class
 m_chain = new TChain(treeName);

 // loop through the input files and add them to the chain
 for(auto inputFile : inputFiles) {
   m_chain->Add(inputFile);
   std::cout << "Added file: " << inputFile << std::endl;
 }

 // create an instance of the Data class. Here the variables
 // are linked with the tree using the SetBranchAddressmethod
 m_data = new Data(m_chain);

 // initialise the algorithms
 for(auto algorithm : algorithms) {
  algorithm->initialize(m_data);
 }
}

void EventLoop::execute() {
 // sanity check. m_chain must not be zero
 if(!m_chain) {
   throw std::runtime_error("Calling execute while the event loop was not initialized.");
 }

 // here we do the actual event loop
 for(int i=0; i<m_chain->GetEntries(); ++i) {
   // event number printout
   if(i%1000==0) {
     std::cout << "Event " << i << std::endl;
   }

   // read the data for i-th event
   m_chain->GetEntry(i);

  // execute all the algorithms attached to this event loop
  for(auto algorithm : algorithms) {
    //algorithm->p_sampleWeight = m_sampleWeight;
    //algorithm->p_isMC = m_isMC;
    algorithm->execute();
  }

 }

}

/*
std::string EventLoop::campaign() { //FIXME check this implementation
  std::string campaign = "";
  for(int i=0; i<1000; ++i)
  {
    if(m_data->ejets_2015 || m_data->mujets_2015)){
      campaign = "MC16a";
      break;
    }
    if(m_data->ejets_2016 || m_data->mujets_2016){
      campaign = "MC16a";
      break;
    }
    if(m_data->ejets_2017 || m_data->mujets_2017){
      campaign = "MC16d";
      break;
    }
    if(m_data->ejets_2018 || m_data->mujets_2018){
      campaign = "MC16e";
      break;
    }
    else{
      std::cout<<"******************* ERROR:: could not determine campaign ******************"<<std::endl;
    }
  }
  return campaign;
}

bool EventLoop::isMC() {
  for(int i=0; i<10; ++i){
    if(m_data->mcChannelNumber != 0) return true;
  }

}
*/