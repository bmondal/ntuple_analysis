from time import sleep
import subprocess

class Jobs(object):
  """ Execute event loops in parallel processes
  """
  def __init__(self, eventLoops):
    """ Constructor. Generates parallel jobs for the provided eventloops
    """
    self.jobNames = []
    for eventLoop in eventLoops:
      self.createExecutable(eventLoop)

  def createExecutable(self, eventLoop):
    """ for a given event loop, generate stand-alone python script
    """
    jobName = "job.{}.py".format(eventLoop.name)

    # open py file for writing
    with open(jobName, "w") as f:
      # create common steering script preamble
      f.write('import sys\n')
      f.write("sys.path.insert(1, '/afs/cern.ch/user/b/bmondal/work/ttgamma-analysis/analysis_framework/source')\n")
      f.write('from ROOT import gSystem, TH1\n')
      f.write('TH1.AddDirectory(False)\n')
      f.write('gSystem.Load("../build/libAnalysis.so")\n')
      f.write('from Samples import *\n')
      f.write('from Algorithms import * \n')

      # create EventLoop class
      className = eventLoop.__class__.__name__
      f.write('eventLoop = {}()\n'.format(className))

      # create algorithm classes
      f.write('algs = []\n')
      for alg in eventLoop.algs:
        algClassName = alg.__class__.__name__
        f.write('algs += [ {}() ]\n'.format(algClassName))
        f.write('eventLoop.addAlgorithms( algs )\n')

      # execute and save calls
      f.write('eventLoop.execute()\n')
      f.write('eventLoop.save()\n')
      f.write('print "all OK"\n')

      # save the job file name for further use
      print "Generated job executable {}".format(jobName)
      self.jobNames += [ jobName ]

  def execute(self, nProcesses=5, niceness=10):
    """ Executes parallel jobs
        nProcesses - max number of parallel processes
        niceness - how nice we are to other users of the computer.
    """
    jobsToSubmit = self.jobNames[:] # copy the list of jobs
    runningProcesses = []
    msg = ""
    while True:
      # printout
      newMsg = "{} jobs out of {} running...".format(len(runningProcesses), len(self.jobNames))
      if newMsg!=msg:
        print newMsg
        msg = newMsg

      # execute new job. Will only happen if number of running jobs is less than nProcesses
      if len(jobsToSubmit) > 0 and len(runningProcesses) < nProcesses:
        command = "python {} > {}.log 2>&1".format(jobsToSubmit[0], jobsToSubmit[0])
        print "Submitting job '{}'".format(jobsToSubmit[0])
        proc = subprocess.Popen("nice -n {} {}".format(niceness,command), shell=True)
        runningProcesses += [ proc ]
        jobsToSubmit.remove(jobsToSubmit[0])

      # check how many jobs running
      for proc in runningProcesses[:]:
        if proc.poll() != None:
          # this process has terminated. Remove from the running
          runningProcesses.remove(proc)

      # exit the loop if all is submitted/done
      if len(jobsToSubmit)+len(runningProcesses)==0:
        print "Done"
        break

      # Wait if not in the submission stage
      if len(jobsToSubmit)==0 or len(runningProcesses) >= nProcesses:
        sleep(1)
