#!/bin/bash
shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS 
lsetup cmake
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"

echo "############################ exporting binaries and scripts  ###########################################"
thisDir=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export PATH=$PATH:$thisDir/../build/:$thisDir:
echo $thisDir
