from Algorithm import Algorithm
from ROOT import TH1D

# -----------------------------------
class AlgDefault(Algorithm):
 """ Default set of histograms
 """
 def __init__(self, name = "AlgDefault"):
  # call inherited constructor
  Algorithm.__init__(self, name)

  # create histograms
  self.alg.h_ph_pt= TH1D("ph_pt", ";#it{pT}_{#gamma} [GeV];Events", 50, 0, 500)
  self.alg.h_ph_eta= TH1D("ph_eta", ";#it{#eta}_{#gamma} [GeV];Events", 12, -3, 3)
  self.alg.h_delta_z_pv = TH1D("delta_z_pv", ";(z_{PV} - z_{#gamma})/#Delta z_{#gamma};Events",40, -10.0, 10.0)
  self.alg.h_z_pv = TH1D("z_pv", ";PV_z;Events",150, -150.0, 150.0)
  self.alg.h_z_ph = TH1D("z_ph", ";ph_z;Events",500, -500.0, 500.0)

# -----------------------------------
class AlgMu20(AlgDefault):
 """ set of histograms with fine binning
 """
 def __init__(self, name="AlgMu20"):
  # call inherited constructor
  AlgDefault.__init__(self, name)

	# apply mu cut
  self.alg.is_mu_cut = True
  self.alg.cut_mu_min.set(0)
  self.alg.cut_mu_max.set(25)

# -----------------------------------
class AlgMu30(AlgDefault):
 """ set of histograms with fine binning
 """
 def __init__(self, name = "AlgMu30"):
  # call inherited constructor
  AlgDefault.__init__(self, name)

	# apply mu cut
  self.alg.is_mu_cut = True
  self.alg.cut_mu_min.set(25)
  self.alg.cut_mu_max.set(35)

# -----------------------------------
class AlgMu50(AlgDefault):
 """ set of histograms with fine binning
 """
 def __init__(self, name = "AlgMu50"):
  # call inherited constructor
  AlgDefault.__init__(self, name)

	# apply mu cut
  self.alg.is_mu_cut = True
  self.alg.cut_mu_min.set(35)
  self.alg.cut_mu_max.set(60)

