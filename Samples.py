from EventLoop import EventLoop

# -----------------------------------
class SampleData(EventLoop):
  # """
  #	event loop over the data samples
  # """
  def __init__(self):
    # call the inherited constructor
    EventLoop.__init__(self, "data")

    # add the data samples into the event loop
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2015.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2016.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2017.1.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2017.2.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2018.1.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/data.AllYear.2018.2.root')


class SampleTtyProdMCaMCd(EventLoop):
  def __init__(self):
    # call the inherited constructor
    EventLoop.__init__(self, "ttyProd_mca_mcd")

    # add the ttyprod_mca samples into the event loop
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_ttgamma_NLO_prod.412112.root')
    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_ttgamma_NLO_prod.412112.root')

    # this is MC sample
    self.isMC = True


# for tty production mc16e samples
class SampleTtyProdMCe(EventLoop):
  def __init__(self):
    # call the inherited constructor
    EventLoop.__init__(self, "ttyProd_mce")

    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttgamma_NLO_prod.412112.0.root')
    self.eventLoop.inputFiles.push_back(
    '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttgamma_NLO_prod.412112.1.root')
    self.eventLoop.inputFiles.push_back(
    '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttgamma_NLO_prod.412112.2.root')

    # this is MC16e sample
    self.isMC = True
    self.isMC16e_sample = True


# for tty decay samples
class SampleTtyDec(EventLoop):
  def __init__(self):
    # call the inherited constructor
    EventLoop.__init__(self, "ttyDec")

    self.eventLoop.inputFiles.push_back(
      '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_ttgamma_LO_dec.412114.root')
    self.eventLoop.inputFiles.push_back(
  '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_ttgamma_LO_dec.412114.root')
    self.eventLoop.inputFiles.push_back(
  '/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttgamma_LO_dec.412114.root')

    # this is tty decay sample
    self.isMC = True
    self.isMC_decay_sample = True


# background samples
class SampleBkg(EventLoop):
  def __init__(self):
    EventLoop.__init__(self, "Bkg")

    # diboson
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_diboson.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_diboson.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_diboson.merged.root')
    # ttV
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_ttV.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_ttV.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttV.merged.root')
    # singletop
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_singletop_schan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_singletop_tchan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_singletop_Wt_inclusive_AFII.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_singletop_schan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_singletop_tchan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_singletop_Wt_inclusive_AFII.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_singletop_schan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_singletop_tchan.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_singletop_Wt_inclusive_AFII.merged.root')
    # Zjets
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_Zjets.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_Zjets.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_Zjets.merged.root')
    # Zgamma
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_Zgamma.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_Zgamma.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_Zgamma.merged.root')
    # Wjets
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_Wjets.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_Wjets.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_Wjets.merged.root')
    # Wgamma
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_Wgamma.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_Wgamma.merged.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_Wgamma.merged.root')
    # ttbar
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16a_TOPQ1_ttbar_FS.410470.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16d_TOPQ1_ttbar_FS.410470.root')
    self.eventLoop.inputFiles.push_back('/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/ljets/mc16e_TOPQ1_ttbar_FS.410470.root')

    # set sample info
    self.isMC = True

