//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct 30 14:33:27 2020 by ROOT version 6.22/02
// from TTree nominal/tree
// found on file: mc16a_TOPQ1_diboson.merged.root
//////////////////////////////////////////////////////////

#ifndef nominal_tree_h
#define nominal_tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class nominal_tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_bTagSF_DL1r_85;
   Float_t         weight_bTagSF_DL1r_77;
   Float_t         weight_bTagSF_DL1r_70;
   Float_t         weight_jvt;
   Float_t         weight_photonSF_effIso;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *ph_iso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<int>     *jet_truthflavExtended;
   vector<char>    *jet_isbtagged_DL1r_85;
   vector<char>    *jet_isbtagged_DL1r_77;
   vector<char>    *jet_isbtagged_DL1r_70;
   Float_t         met_met;
   Float_t         met_phi;
   vector<float>   *klfitter_logLikelihood;
   vector<float>   *klfitter_eventProbability;
   Short_t         klfitter_selected;
   vector<string>  *klfitter_selection;
   vector<short>   *klfitter_minuitDidNotConverge;
   vector<short>   *klfitter_fitAbortedDueToNaN;
   vector<short>   *klfitter_atLeastOneFitParameterAtItsLimit;
   vector<short>   *klfitter_invalidTransferFunctionAtConvergence;
   vector<unsigned int> *klfitter_parameters_size;
   vector<vector<double> > *klfitter_parameters;
   vector<vector<double> > *klfitter_parameterErrors;
   vector<unsigned int> *klfitter_bestPermutation;
   vector<float>   *klfitter_model_bhad_pt;
   vector<float>   *klfitter_model_bhad_eta;
   vector<float>   *klfitter_model_bhad_phi;
   vector<float>   *klfitter_model_bhad_E;
   vector<unsigned int> *klfitter_model_bhad_jetIndex;
   vector<float>   *klfitter_model_blep_pt;
   vector<float>   *klfitter_model_blep_eta;
   vector<float>   *klfitter_model_blep_phi;
   vector<float>   *klfitter_model_blep_E;
   vector<unsigned int> *klfitter_model_blep_jetIndex;
   vector<float>   *klfitter_model_lq1_pt;
   vector<float>   *klfitter_model_lq1_eta;
   vector<float>   *klfitter_model_lq1_phi;
   vector<float>   *klfitter_model_lq1_E;
   vector<unsigned int> *klfitter_model_lq1_jetIndex;
   vector<float>   *klfitter_model_lep_pt;
   vector<float>   *klfitter_model_lep_eta;
   vector<float>   *klfitter_model_lep_phi;
   vector<float>   *klfitter_model_lep_E;
   vector<float>   *klfitter_model_nu_pt;
   vector<float>   *klfitter_model_nu_eta;
   vector<float>   *klfitter_model_nu_phi;
   vector<float>   *klfitter_model_nu_E;
   vector<float>   *klfitter_model_lq2_pt;
   vector<float>   *klfitter_model_lq2_eta;
   vector<float>   *klfitter_model_lq2_phi;
   vector<float>   *klfitter_model_lq2_E;
   vector<unsigned int> *klfitter_model_lq2_jetIndex;
   Float_t         klfitter_bestPerm_topLep_pt;
   Float_t         klfitter_bestPerm_topLep_eta;
   Float_t         klfitter_bestPerm_topLep_phi;
   Float_t         klfitter_bestPerm_topLep_E;
   Float_t         klfitter_bestPerm_topLep_m;
   Float_t         klfitter_bestPerm_topHad_pt;
   Float_t         klfitter_bestPerm_topHad_eta;
   Float_t         klfitter_bestPerm_topHad_phi;
   Float_t         klfitter_bestPerm_topHad_E;
   Float_t         klfitter_bestPerm_topHad_m;
   Float_t         klfitter_bestPerm_ttbar_pt;
   Float_t         klfitter_bestPerm_ttbar_eta;
   Float_t         klfitter_bestPerm_ttbar_phi;
   Float_t         klfitter_bestPerm_ttbar_E;
   Float_t         klfitter_bestPerm_ttbar_m;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           ejets_2017;
   Int_t           ejets_2018;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           mujets_2017;
   Int_t           mujets_2018;
   Float_t         event_dphill;
   Float_t         event_detall;
   Float_t         event_drll;
   Float_t         event_mll;
   Float_t         event_mwt;
   Float_t         event_HT;
   Float_t         event_ST;
   Int_t           event_njets;
   Int_t           event_nbjets70;
   Int_t           event_nbjets77;
   Int_t           event_nbjets85;
   Char_t          event_in_overlap;
   Char_t          event_in_overlap_TTH;
   Char_t          event_in_overlap_REL20;
   vector<int>     *ph_good_index;
   vector<int>     *ph_true_category;
   vector<char>    *ph_true_topparent;
   vector<int>     *ph_true_parent;
   vector<int>     *ph_true_barcode;
   vector<int>     *ph_true_origin;
   vector<int>     *ph_true_pdgid;
   vector<int>     *ph_true_type;
   vector<float>   *ph_true_eta;
   vector<float>   *ph_true_phi;
   vector<float>   *ph_true_pt;
   vector<int>     *ph_conversion_type;
   vector<float>   *ph_drmuph;
   vector<float>   *ph_drelph;
   vector<float>   *ph_drlph_leading;
   vector<float>   *ph_drlph_subleading;
   vector<float>   *ph_drlph_closest;
   vector<float>   *ph_mlph_closest;
   vector<float>   *ph_mllph;
   vector<char>    *ph_id_loose;
   vector<char>    *ph_id_tight;
   vector<char>    *ph_id_tight_relaxed;
   vector<char>    *ph_id_fail_deltaE;
   vector<char>    *ph_id_fail_fside;
   vector<char>    *ph_id_fail_ws3;
   vector<char>    *ph_id_fail_Eratio;
   vector<int>     *ph_id_fail_count;
   vector<float>   *ph_iso_ptcone20;
   vector<float>   *ph_iso_topoetcone20;
   vector<float>   *ph_iso_topoetcone40;
   vector<char>    *ph_iso_FCL;
   vector<char>    *ph_iso_FCT;
   vector<char>    *ph_iso_FCTCO;
   vector<float>   *ph_ss_deltaE;
   vector<float>   *ph_ss_e277;
   vector<float>   *ph_ss_emaxs1;
   vector<float>   *ph_ss_eratio;
   vector<float>   *ph_ss_f1;
   vector<float>   *ph_ss_fracs1;
   vector<float>   *ph_ss_reta;
   vector<float>   *ph_ss_rhad;
   vector<float>   *ph_ss_rhad1;
   vector<float>   *ph_ss_rphi;
   vector<float>   *ph_ss_weta1;
   vector<float>   *ph_ss_weta2;
   vector<float>   *ph_ss_wtots1;
   vector<float>   *ph_drphb70_leading_pt;
   vector<float>   *ph_drphb77_leading_pt;
   vector<float>   *ph_drphb85_leading_pt;
   vector<float>   *ph_drphb70_subleading_pt;
   vector<float>   *ph_drphb77_subleading_pt;
   vector<float>   *ph_drphb85_subleading_pt;
   vector<float>   *ph_drphb70_leading_score;
   vector<float>   *ph_drphb77_leading_score;
   vector<float>   *ph_drphb85_leading_score;
   vector<float>   *ph_drphb70_subleading_score;
   vector<float>   *ph_drphb77_subleading_score;
   vector<float>   *ph_drphb85_subleading_score;
   vector<float>   *ph_drphb70_closest;
   vector<float>   *ph_drphb77_closest;
   vector<float>   *ph_drphb85_closest;
   vector<float>   *ph_mphb70_closest;
   vector<float>   *ph_mphb77_closest;
   vector<float>   *ph_mphb85_closest;
   vector<float>   *el_mlb70;
   vector<float>   *el_mlb77;
   vector<float>   *el_mlb85;
   vector<float>   *el_drlb70;
   vector<float>   *el_drlb77;
   vector<float>   *el_drlb85;
   vector<float>   *mu_mlb70;
   vector<float>   *mu_mlb77;
   vector<float>   *mu_mlb85;
   vector<float>   *mu_drlb70;
   vector<float>   *mu_drlb77;
   vector<float>   *mu_drlb85;
   vector<int>     *jet_mv2c10_ranked_index;
   Float_t         event_PV_z;
   vector<float>   *ph_z;
   vector<float>   *ph_dz;
   vector<float>   *ph_dz_ratio;
   Float_t         event_lumi;
   Float_t         event_norm;
   vector<int>     *ph_medium_index;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_bTagSF_DL1r_85;   //!
   TBranch        *b_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_weight_bTagSF_DL1r_70;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_iso;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_truthflavExtended;   //!
   TBranch        *b_jet_isbtagged_DL1r_85;   //!
   TBranch        *b_jet_isbtagged_DL1r_77;   //!
   TBranch        *b_jet_isbtagged_DL1r_70;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_klfitter_logLikelihood;   //!
   TBranch        *b_klfitter_eventProbability;   //!
   TBranch        *b_klfitter_selected;   //!
   TBranch        *b_klfitter_selection;   //!
   TBranch        *b_klfitter_minuitDidNotConverge;   //!
   TBranch        *b_klfitter_fitAbortedDueToNaN;   //!
   TBranch        *b_klfitter_atLeastOneFitParameterAtItsLimit;   //!
   TBranch        *b_klfitter_invalidTransferFunctionAtConvergence;   //!
   TBranch        *b_klfitter_parameters_size;   //!
   TBranch        *b_klfitter_parameters;   //!
   TBranch        *b_klfitter_parameterErrors;   //!
   TBranch        *b_klfitter_bestPermutation;   //!
   TBranch        *b_klfitter_model_bhad_pt;   //!
   TBranch        *b_klfitter_model_bhad_eta;   //!
   TBranch        *b_klfitter_model_bhad_phi;   //!
   TBranch        *b_klfitter_model_bhad_E;   //!
   TBranch        *b_klfitter_model_bhad_jetIndex;   //!
   TBranch        *b_klfitter_model_blep_pt;   //!
   TBranch        *b_klfitter_model_blep_eta;   //!
   TBranch        *b_klfitter_model_blep_phi;   //!
   TBranch        *b_klfitter_model_blep_E;   //!
   TBranch        *b_klfitter_model_blep_jetIndex;   //!
   TBranch        *b_klfitter_model_lq1_pt;   //!
   TBranch        *b_klfitter_model_lq1_eta;   //!
   TBranch        *b_klfitter_model_lq1_phi;   //!
   TBranch        *b_klfitter_model_lq1_E;   //!
   TBranch        *b_klfitter_model_lq1_jetIndex;   //!
   TBranch        *b_klfitter_model_lep_pt;   //!
   TBranch        *b_klfitter_model_lep_eta;   //!
   TBranch        *b_klfitter_model_lep_phi;   //!
   TBranch        *b_klfitter_model_lep_E;   //!
   TBranch        *b_klfitter_model_nu_pt;   //!
   TBranch        *b_klfitter_model_nu_eta;   //!
   TBranch        *b_klfitter_model_nu_phi;   //!
   TBranch        *b_klfitter_model_nu_E;   //!
   TBranch        *b_klfitter_model_lq2_pt;   //!
   TBranch        *b_klfitter_model_lq2_eta;   //!
   TBranch        *b_klfitter_model_lq2_phi;   //!
   TBranch        *b_klfitter_model_lq2_E;   //!
   TBranch        *b_klfitter_model_lq2_jetIndex;   //!
   TBranch        *b_klfitter_bestPerm_topLep_pt;   //!
   TBranch        *b_klfitter_bestPerm_topLep_eta;   //!
   TBranch        *b_klfitter_bestPerm_topLep_phi;   //!
   TBranch        *b_klfitter_bestPerm_topLep_E;   //!
   TBranch        *b_klfitter_bestPerm_topLep_m;   //!
   TBranch        *b_klfitter_bestPerm_topHad_pt;   //!
   TBranch        *b_klfitter_bestPerm_topHad_eta;   //!
   TBranch        *b_klfitter_bestPerm_topHad_phi;   //!
   TBranch        *b_klfitter_bestPerm_topHad_E;   //!
   TBranch        *b_klfitter_bestPerm_topHad_m;   //!
   TBranch        *b_klfitter_bestPerm_ttbar_pt;   //!
   TBranch        *b_klfitter_bestPerm_ttbar_eta;   //!
   TBranch        *b_klfitter_bestPerm_ttbar_phi;   //!
   TBranch        *b_klfitter_bestPerm_ttbar_E;   //!
   TBranch        *b_klfitter_bestPerm_ttbar_m;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_2017;   //!
   TBranch        *b_ejets_2018;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_2017;   //!
   TBranch        *b_mujets_2018;   //!
   TBranch        *b_event_dphill;   //!
   TBranch        *b_event_detall;   //!
   TBranch        *b_event_drll;   //!
   TBranch        *b_event_mll;   //!
   TBranch        *b_event_mwt;   //!
   TBranch        *b_event_HT;   //!
   TBranch        *b_event_ST;   //!
   TBranch        *b_event_njets;   //!
   TBranch        *b_event_nbjets70;   //!
   TBranch        *b_event_nbjets77;   //!
   TBranch        *b_event_nbjets85;   //!
   TBranch        *b_event_in_overlap;   //!
   TBranch        *b_event_in_overlap_TTH;   //!
   TBranch        *b_event_in_overlap_REL20;   //!
   TBranch        *b_ph_good_index;   //!
   TBranch        *b_ph_true_category;   //!
   TBranch        *b_ph_true_topparent;   //!
   TBranch        *b_ph_true_parent;   //!
   TBranch        *b_ph_true_barcode;   //!
   TBranch        *b_ph_true_origin;   //!
   TBranch        *b_ph_true_pdgid;   //!
   TBranch        *b_ph_true_type;   //!
   TBranch        *b_ph_true_eta;   //!
   TBranch        *b_ph_true_phi;   //!
   TBranch        *b_ph_true_pt;   //!
   TBranch        *b_ph_conversion_type;   //!
   TBranch        *b_ph_drmuph;   //!
   TBranch        *b_ph_drelph;   //!
   TBranch        *b_ph_drlph_leading;   //!
   TBranch        *b_ph_drlph_subleading;   //!
   TBranch        *b_ph_drlph_closest;   //!
   TBranch        *b_ph_mlph_closest;   //!
   TBranch        *b_ph_mllph;   //!
   TBranch        *b_ph_id_loose;   //!
   TBranch        *b_ph_id_tight;   //!
   TBranch        *b_ph_id_tight_relaxed;   //!
   TBranch        *b_ph_id_fail_deltaE;   //!
   TBranch        *b_ph_id_fail_fside;   //!
   TBranch        *b_ph_id_fail_ws3;   //!
   TBranch        *b_ph_id_fail_Eratio;   //!
   TBranch        *b_ph_id_fail_count;   //!
   TBranch        *b_ph_iso_ptcone20;   //!
   TBranch        *b_ph_iso_topoetcone20;   //!
   TBranch        *b_ph_iso_topoetcone40;   //!
   TBranch        *b_ph_iso_FCL;   //!
   TBranch        *b_ph_iso_FCT;   //!
   TBranch        *b_ph_iso_FCTCO;   //!
   TBranch        *b_ph_ss_deltaE;   //!
   TBranch        *b_ph_ss_e277;   //!
   TBranch        *b_ph_ss_emaxs1;   //!
   TBranch        *b_ph_ss_eratio;   //!
   TBranch        *b_ph_ss_f1;   //!
   TBranch        *b_ph_ss_fracs1;   //!
   TBranch        *b_ph_ss_reta;   //!
   TBranch        *b_ph_ss_rhad;   //!
   TBranch        *b_ph_ss_rhad1;   //!
   TBranch        *b_ph_ss_rphi;   //!
   TBranch        *b_ph_ss_weta1;   //!
   TBranch        *b_ph_ss_weta2;   //!
   TBranch        *b_ph_ss_wtots1;   //!
   TBranch        *b_ph_drphb70_leading_pt;   //!
   TBranch        *b_ph_drphb77_leading_pt;   //!
   TBranch        *b_ph_drphb85_leading_pt;   //!
   TBranch        *b_ph_drphb70_subleading_pt;   //!
   TBranch        *b_ph_drphb77_subleading_pt;   //!
   TBranch        *b_ph_drphb85_subleading_pt;   //!
   TBranch        *b_ph_drphb70_leading_score;   //!
   TBranch        *b_ph_drphb77_leading_score;   //!
   TBranch        *b_ph_drphb85_leading_score;   //!
   TBranch        *b_ph_drphb70_subleading_score;   //!
   TBranch        *b_ph_drphb77_subleading_score;   //!
   TBranch        *b_ph_drphb85_subleading_score;   //!
   TBranch        *b_ph_drphb70_closest;   //!
   TBranch        *b_ph_drphb77_closest;   //!
   TBranch        *b_ph_drphb85_closest;   //!
   TBranch        *b_ph_mphb70_closest;   //!
   TBranch        *b_ph_mphb77_closest;   //!
   TBranch        *b_ph_mphb85_closest;   //!
   TBranch        *b_el_mlb70;   //!
   TBranch        *b_el_mlb77;   //!
   TBranch        *b_el_mlb85;   //!
   TBranch        *b_el_drlb70;   //!
   TBranch        *b_el_drlb77;   //!
   TBranch        *b_el_drlb85;   //!
   TBranch        *b_mu_mlb70;   //!
   TBranch        *b_mu_mlb77;   //!
   TBranch        *b_mu_mlb85;   //!
   TBranch        *b_mu_drlb70;   //!
   TBranch        *b_mu_drlb77;   //!
   TBranch        *b_mu_drlb85;   //!
   TBranch        *b_jet_mv2c10_ranked_index;   //!
   TBranch        *b_event_PV_z;   //!
   TBranch        *b_ph_z;   //!
   TBranch        *b_ph_dz;   //!
   TBranch        *b_ph_dz_ratio;   //!
   TBranch        *b_event_lumi;   //!
   TBranch        *b_event_norm;   //!
   TBranch        *b_ph_medium_index;   //!

   nominal_tree(TTree *tree=0);
   virtual ~nominal_tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef nominal_tree_cxx
nominal_tree::nominal_tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("mc16a_TOPQ1_diboson.merged.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("mc16a_TOPQ1_diboson.merged.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

nominal_tree::~nominal_tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t nominal_tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t nominal_tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void nominal_tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_e = 0;
   ph_iso = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c10 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_truthflavExtended = 0;
   jet_isbtagged_DL1r_85 = 0;
   jet_isbtagged_DL1r_77 = 0;
   jet_isbtagged_DL1r_70 = 0;
   klfitter_logLikelihood = 0;
   klfitter_eventProbability = 0;
   klfitter_selection = 0;
   klfitter_minuitDidNotConverge = 0;
   klfitter_fitAbortedDueToNaN = 0;
   klfitter_atLeastOneFitParameterAtItsLimit = 0;
   klfitter_invalidTransferFunctionAtConvergence = 0;
   klfitter_parameters_size = 0;
   klfitter_parameters = 0;
   klfitter_parameterErrors = 0;
   klfitter_bestPermutation = 0;
   klfitter_model_bhad_pt = 0;
   klfitter_model_bhad_eta = 0;
   klfitter_model_bhad_phi = 0;
   klfitter_model_bhad_E = 0;
   klfitter_model_bhad_jetIndex = 0;
   klfitter_model_blep_pt = 0;
   klfitter_model_blep_eta = 0;
   klfitter_model_blep_phi = 0;
   klfitter_model_blep_E = 0;
   klfitter_model_blep_jetIndex = 0;
   klfitter_model_lq1_pt = 0;
   klfitter_model_lq1_eta = 0;
   klfitter_model_lq1_phi = 0;
   klfitter_model_lq1_E = 0;
   klfitter_model_lq1_jetIndex = 0;
   klfitter_model_lep_pt = 0;
   klfitter_model_lep_eta = 0;
   klfitter_model_lep_phi = 0;
   klfitter_model_lep_E = 0;
   klfitter_model_nu_pt = 0;
   klfitter_model_nu_eta = 0;
   klfitter_model_nu_phi = 0;
   klfitter_model_nu_E = 0;
   klfitter_model_lq2_pt = 0;
   klfitter_model_lq2_eta = 0;
   klfitter_model_lq2_phi = 0;
   klfitter_model_lq2_E = 0;
   klfitter_model_lq2_jetIndex = 0;
   ph_good_index = 0;
   ph_true_category = 0;
   ph_true_topparent = 0;
   ph_true_parent = 0;
   ph_true_barcode = 0;
   ph_true_origin = 0;
   ph_true_pdgid = 0;
   ph_true_type = 0;
   ph_true_eta = 0;
   ph_true_phi = 0;
   ph_true_pt = 0;
   ph_conversion_type = 0;
   ph_drmuph = 0;
   ph_drelph = 0;
   ph_drlph_leading = 0;
   ph_drlph_subleading = 0;
   ph_drlph_closest = 0;
   ph_mlph_closest = 0;
   ph_mllph = 0;
   ph_id_loose = 0;
   ph_id_tight = 0;
   ph_id_tight_relaxed = 0;
   ph_id_fail_deltaE = 0;
   ph_id_fail_fside = 0;
   ph_id_fail_ws3 = 0;
   ph_id_fail_Eratio = 0;
   ph_id_fail_count = 0;
   ph_iso_ptcone20 = 0;
   ph_iso_topoetcone20 = 0;
   ph_iso_topoetcone40 = 0;
   ph_iso_FCL = 0;
   ph_iso_FCT = 0;
   ph_iso_FCTCO = 0;
   ph_ss_deltaE = 0;
   ph_ss_e277 = 0;
   ph_ss_emaxs1 = 0;
   ph_ss_eratio = 0;
   ph_ss_f1 = 0;
   ph_ss_fracs1 = 0;
   ph_ss_reta = 0;
   ph_ss_rhad = 0;
   ph_ss_rhad1 = 0;
   ph_ss_rphi = 0;
   ph_ss_weta1 = 0;
   ph_ss_weta2 = 0;
   ph_ss_wtots1 = 0;
   ph_drphb70_leading_pt = 0;
   ph_drphb77_leading_pt = 0;
   ph_drphb85_leading_pt = 0;
   ph_drphb70_subleading_pt = 0;
   ph_drphb77_subleading_pt = 0;
   ph_drphb85_subleading_pt = 0;
   ph_drphb70_leading_score = 0;
   ph_drphb77_leading_score = 0;
   ph_drphb85_leading_score = 0;
   ph_drphb70_subleading_score = 0;
   ph_drphb77_subleading_score = 0;
   ph_drphb85_subleading_score = 0;
   ph_drphb70_closest = 0;
   ph_drphb77_closest = 0;
   ph_drphb85_closest = 0;
   ph_mphb70_closest = 0;
   ph_mphb77_closest = 0;
   ph_mphb85_closest = 0;
   el_mlb70 = 0;
   el_mlb77 = 0;
   el_mlb85 = 0;
   el_drlb70 = 0;
   el_drlb77 = 0;
   el_drlb85 = 0;
   mu_mlb70 = 0;
   mu_mlb77 = 0;
   mu_mlb85 = 0;
   mu_drlb70 = 0;
   mu_drlb77 = 0;
   mu_drlb85 = 0;
   jet_mv2c10_ranked_index = 0;
   ph_z = 0;
   ph_dz = 0;
   ph_dz_ratio = 0;
   ph_medium_index = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_photonSF", &weight_photonSF, &b_weight_photonSF);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85", &weight_bTagSF_DL1r_85, &b_weight_bTagSF_DL1r_85);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70", &weight_bTagSF_DL1r_70, &b_weight_bTagSF_DL1r_70);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso, &b_weight_photonSF_effIso);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);
   fChain->SetBranchAddress("ph_iso", &ph_iso, &b_ph_iso);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
   fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_85", &jet_isbtagged_DL1r_85, &b_jet_isbtagged_DL1r_85);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_77", &jet_isbtagged_DL1r_77, &b_jet_isbtagged_DL1r_77);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_70", &jet_isbtagged_DL1r_70, &b_jet_isbtagged_DL1r_70);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("klfitter_logLikelihood", &klfitter_logLikelihood, &b_klfitter_logLikelihood);
   fChain->SetBranchAddress("klfitter_eventProbability", &klfitter_eventProbability, &b_klfitter_eventProbability);
   fChain->SetBranchAddress("klfitter_selected", &klfitter_selected, &b_klfitter_selected);
   fChain->SetBranchAddress("klfitter_selection", &klfitter_selection, &b_klfitter_selection);
   fChain->SetBranchAddress("klfitter_minuitDidNotConverge", &klfitter_minuitDidNotConverge, &b_klfitter_minuitDidNotConverge);
   fChain->SetBranchAddress("klfitter_fitAbortedDueToNaN", &klfitter_fitAbortedDueToNaN, &b_klfitter_fitAbortedDueToNaN);
   fChain->SetBranchAddress("klfitter_atLeastOneFitParameterAtItsLimit", &klfitter_atLeastOneFitParameterAtItsLimit, &b_klfitter_atLeastOneFitParameterAtItsLimit);
   fChain->SetBranchAddress("klfitter_invalidTransferFunctionAtConvergence", &klfitter_invalidTransferFunctionAtConvergence, &b_klfitter_invalidTransferFunctionAtConvergence);
   fChain->SetBranchAddress("klfitter_parameters_size", &klfitter_parameters_size, &b_klfitter_parameters_size);
   fChain->SetBranchAddress("klfitter_parameters", &klfitter_parameters, &b_klfitter_parameters);
   fChain->SetBranchAddress("klfitter_parameterErrors", &klfitter_parameterErrors, &b_klfitter_parameterErrors);
   fChain->SetBranchAddress("klfitter_bestPermutation", &klfitter_bestPermutation, &b_klfitter_bestPermutation);
   fChain->SetBranchAddress("klfitter_model_bhad_pt", &klfitter_model_bhad_pt, &b_klfitter_model_bhad_pt);
   fChain->SetBranchAddress("klfitter_model_bhad_eta", &klfitter_model_bhad_eta, &b_klfitter_model_bhad_eta);
   fChain->SetBranchAddress("klfitter_model_bhad_phi", &klfitter_model_bhad_phi, &b_klfitter_model_bhad_phi);
   fChain->SetBranchAddress("klfitter_model_bhad_E", &klfitter_model_bhad_E, &b_klfitter_model_bhad_E);
   fChain->SetBranchAddress("klfitter_model_bhad_jetIndex", &klfitter_model_bhad_jetIndex, &b_klfitter_model_bhad_jetIndex);
   fChain->SetBranchAddress("klfitter_model_blep_pt", &klfitter_model_blep_pt, &b_klfitter_model_blep_pt);
   fChain->SetBranchAddress("klfitter_model_blep_eta", &klfitter_model_blep_eta, &b_klfitter_model_blep_eta);
   fChain->SetBranchAddress("klfitter_model_blep_phi", &klfitter_model_blep_phi, &b_klfitter_model_blep_phi);
   fChain->SetBranchAddress("klfitter_model_blep_E", &klfitter_model_blep_E, &b_klfitter_model_blep_E);
   fChain->SetBranchAddress("klfitter_model_blep_jetIndex", &klfitter_model_blep_jetIndex, &b_klfitter_model_blep_jetIndex);
   fChain->SetBranchAddress("klfitter_model_lq1_pt", &klfitter_model_lq1_pt, &b_klfitter_model_lq1_pt);
   fChain->SetBranchAddress("klfitter_model_lq1_eta", &klfitter_model_lq1_eta, &b_klfitter_model_lq1_eta);
   fChain->SetBranchAddress("klfitter_model_lq1_phi", &klfitter_model_lq1_phi, &b_klfitter_model_lq1_phi);
   fChain->SetBranchAddress("klfitter_model_lq1_E", &klfitter_model_lq1_E, &b_klfitter_model_lq1_E);
   fChain->SetBranchAddress("klfitter_model_lq1_jetIndex", &klfitter_model_lq1_jetIndex, &b_klfitter_model_lq1_jetIndex);
   fChain->SetBranchAddress("klfitter_model_lep_pt", &klfitter_model_lep_pt, &b_klfitter_model_lep_pt);
   fChain->SetBranchAddress("klfitter_model_lep_eta", &klfitter_model_lep_eta, &b_klfitter_model_lep_eta);
   fChain->SetBranchAddress("klfitter_model_lep_phi", &klfitter_model_lep_phi, &b_klfitter_model_lep_phi);
   fChain->SetBranchAddress("klfitter_model_lep_E", &klfitter_model_lep_E, &b_klfitter_model_lep_E);
   fChain->SetBranchAddress("klfitter_model_nu_pt", &klfitter_model_nu_pt, &b_klfitter_model_nu_pt);
   fChain->SetBranchAddress("klfitter_model_nu_eta", &klfitter_model_nu_eta, &b_klfitter_model_nu_eta);
   fChain->SetBranchAddress("klfitter_model_nu_phi", &klfitter_model_nu_phi, &b_klfitter_model_nu_phi);
   fChain->SetBranchAddress("klfitter_model_nu_E", &klfitter_model_nu_E, &b_klfitter_model_nu_E);
   fChain->SetBranchAddress("klfitter_model_lq2_pt", &klfitter_model_lq2_pt, &b_klfitter_model_lq2_pt);
   fChain->SetBranchAddress("klfitter_model_lq2_eta", &klfitter_model_lq2_eta, &b_klfitter_model_lq2_eta);
   fChain->SetBranchAddress("klfitter_model_lq2_phi", &klfitter_model_lq2_phi, &b_klfitter_model_lq2_phi);
   fChain->SetBranchAddress("klfitter_model_lq2_E", &klfitter_model_lq2_E, &b_klfitter_model_lq2_E);
   fChain->SetBranchAddress("klfitter_model_lq2_jetIndex", &klfitter_model_lq2_jetIndex, &b_klfitter_model_lq2_jetIndex);
   fChain->SetBranchAddress("klfitter_bestPerm_topLep_pt", &klfitter_bestPerm_topLep_pt, &b_klfitter_bestPerm_topLep_pt);
   fChain->SetBranchAddress("klfitter_bestPerm_topLep_eta", &klfitter_bestPerm_topLep_eta, &b_klfitter_bestPerm_topLep_eta);
   fChain->SetBranchAddress("klfitter_bestPerm_topLep_phi", &klfitter_bestPerm_topLep_phi, &b_klfitter_bestPerm_topLep_phi);
   fChain->SetBranchAddress("klfitter_bestPerm_topLep_E", &klfitter_bestPerm_topLep_E, &b_klfitter_bestPerm_topLep_E);
   fChain->SetBranchAddress("klfitter_bestPerm_topLep_m", &klfitter_bestPerm_topLep_m, &b_klfitter_bestPerm_topLep_m);
   fChain->SetBranchAddress("klfitter_bestPerm_topHad_pt", &klfitter_bestPerm_topHad_pt, &b_klfitter_bestPerm_topHad_pt);
   fChain->SetBranchAddress("klfitter_bestPerm_topHad_eta", &klfitter_bestPerm_topHad_eta, &b_klfitter_bestPerm_topHad_eta);
   fChain->SetBranchAddress("klfitter_bestPerm_topHad_phi", &klfitter_bestPerm_topHad_phi, &b_klfitter_bestPerm_topHad_phi);
   fChain->SetBranchAddress("klfitter_bestPerm_topHad_E", &klfitter_bestPerm_topHad_E, &b_klfitter_bestPerm_topHad_E);
   fChain->SetBranchAddress("klfitter_bestPerm_topHad_m", &klfitter_bestPerm_topHad_m, &b_klfitter_bestPerm_topHad_m);
   fChain->SetBranchAddress("klfitter_bestPerm_ttbar_pt", &klfitter_bestPerm_ttbar_pt, &b_klfitter_bestPerm_ttbar_pt);
   fChain->SetBranchAddress("klfitter_bestPerm_ttbar_eta", &klfitter_bestPerm_ttbar_eta, &b_klfitter_bestPerm_ttbar_eta);
   fChain->SetBranchAddress("klfitter_bestPerm_ttbar_phi", &klfitter_bestPerm_ttbar_phi, &b_klfitter_bestPerm_ttbar_phi);
   fChain->SetBranchAddress("klfitter_bestPerm_ttbar_E", &klfitter_bestPerm_ttbar_E, &b_klfitter_bestPerm_ttbar_E);
   fChain->SetBranchAddress("klfitter_bestPerm_ttbar_m", &klfitter_bestPerm_ttbar_m, &b_klfitter_bestPerm_ttbar_m);
   fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
   fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
   fChain->SetBranchAddress("ejets_2017", &ejets_2017, &b_ejets_2017);
   fChain->SetBranchAddress("ejets_2018", &ejets_2018, &b_ejets_2018);
   fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
   fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
   fChain->SetBranchAddress("mujets_2017", &mujets_2017, &b_mujets_2017);
   fChain->SetBranchAddress("mujets_2018", &mujets_2018, &b_mujets_2018);
   fChain->SetBranchAddress("event_dphill", &event_dphill, &b_event_dphill);
   fChain->SetBranchAddress("event_detall", &event_detall, &b_event_detall);
   fChain->SetBranchAddress("event_drll", &event_drll, &b_event_drll);
   fChain->SetBranchAddress("event_mll", &event_mll, &b_event_mll);
   fChain->SetBranchAddress("event_mwt", &event_mwt, &b_event_mwt);
   fChain->SetBranchAddress("event_HT", &event_HT, &b_event_HT);
   fChain->SetBranchAddress("event_ST", &event_ST, &b_event_ST);
   fChain->SetBranchAddress("event_njets", &event_njets, &b_event_njets);
   fChain->SetBranchAddress("event_nbjets70", &event_nbjets70, &b_event_nbjets70);
   fChain->SetBranchAddress("event_nbjets77", &event_nbjets77, &b_event_nbjets77);
   fChain->SetBranchAddress("event_nbjets85", &event_nbjets85, &b_event_nbjets85);
   fChain->SetBranchAddress("event_in_overlap", &event_in_overlap, &b_event_in_overlap);
   fChain->SetBranchAddress("event_in_overlap_TTH", &event_in_overlap_TTH, &b_event_in_overlap_TTH);
   fChain->SetBranchAddress("event_in_overlap_REL20", &event_in_overlap_REL20, &b_event_in_overlap_REL20);
   fChain->SetBranchAddress("ph_good_index", &ph_good_index, &b_ph_good_index);
   fChain->SetBranchAddress("ph_true_category", &ph_true_category, &b_ph_true_category);
   fChain->SetBranchAddress("ph_true_topparent", &ph_true_topparent, &b_ph_true_topparent);
   fChain->SetBranchAddress("ph_true_parent", &ph_true_parent, &b_ph_true_parent);
   fChain->SetBranchAddress("ph_true_barcode", &ph_true_barcode, &b_ph_true_barcode);
   fChain->SetBranchAddress("ph_true_origin", &ph_true_origin, &b_ph_true_origin);
   fChain->SetBranchAddress("ph_true_pdgid", &ph_true_pdgid, &b_ph_true_pdgid);
   fChain->SetBranchAddress("ph_true_type", &ph_true_type, &b_ph_true_type);
   fChain->SetBranchAddress("ph_true_eta", &ph_true_eta, &b_ph_true_eta);
   fChain->SetBranchAddress("ph_true_phi", &ph_true_phi, &b_ph_true_phi);
   fChain->SetBranchAddress("ph_true_pt", &ph_true_pt, &b_ph_true_pt);
   fChain->SetBranchAddress("ph_conversion_type", &ph_conversion_type, &b_ph_conversion_type);
   fChain->SetBranchAddress("ph_drmuph", &ph_drmuph, &b_ph_drmuph);
   fChain->SetBranchAddress("ph_drelph", &ph_drelph, &b_ph_drelph);
   fChain->SetBranchAddress("ph_drlph_leading", &ph_drlph_leading, &b_ph_drlph_leading);
   fChain->SetBranchAddress("ph_drlph_subleading", &ph_drlph_subleading, &b_ph_drlph_subleading);
   fChain->SetBranchAddress("ph_drlph_closest", &ph_drlph_closest, &b_ph_drlph_closest);
   fChain->SetBranchAddress("ph_mlph_closest", &ph_mlph_closest, &b_ph_mlph_closest);
   fChain->SetBranchAddress("ph_mllph", &ph_mllph, &b_ph_mllph);
   fChain->SetBranchAddress("ph_id_loose", &ph_id_loose, &b_ph_id_loose);
   fChain->SetBranchAddress("ph_id_tight", &ph_id_tight, &b_ph_id_tight);
   fChain->SetBranchAddress("ph_id_tight_relaxed", &ph_id_tight_relaxed, &b_ph_id_tight_relaxed);
   fChain->SetBranchAddress("ph_id_fail_deltaE", &ph_id_fail_deltaE, &b_ph_id_fail_deltaE);
   fChain->SetBranchAddress("ph_id_fail_fside", &ph_id_fail_fside, &b_ph_id_fail_fside);
   fChain->SetBranchAddress("ph_id_fail_ws3", &ph_id_fail_ws3, &b_ph_id_fail_ws3);
   fChain->SetBranchAddress("ph_id_fail_Eratio", &ph_id_fail_Eratio, &b_ph_id_fail_Eratio);
   fChain->SetBranchAddress("ph_id_fail_count", &ph_id_fail_count, &b_ph_id_fail_count);
   fChain->SetBranchAddress("ph_iso_ptcone20", &ph_iso_ptcone20, &b_ph_iso_ptcone20);
   fChain->SetBranchAddress("ph_iso_topoetcone20", &ph_iso_topoetcone20, &b_ph_iso_topoetcone20);
   fChain->SetBranchAddress("ph_iso_topoetcone40", &ph_iso_topoetcone40, &b_ph_iso_topoetcone40);
   fChain->SetBranchAddress("ph_iso_FCL", &ph_iso_FCL, &b_ph_iso_FCL);
   fChain->SetBranchAddress("ph_iso_FCT", &ph_iso_FCT, &b_ph_iso_FCT);
   fChain->SetBranchAddress("ph_iso_FCTCO", &ph_iso_FCTCO, &b_ph_iso_FCTCO);
   fChain->SetBranchAddress("ph_ss_deltaE", &ph_ss_deltaE, &b_ph_ss_deltaE);
   fChain->SetBranchAddress("ph_ss_e277", &ph_ss_e277, &b_ph_ss_e277);
   fChain->SetBranchAddress("ph_ss_emaxs1", &ph_ss_emaxs1, &b_ph_ss_emaxs1);
   fChain->SetBranchAddress("ph_ss_eratio", &ph_ss_eratio, &b_ph_ss_eratio);
   fChain->SetBranchAddress("ph_ss_f1", &ph_ss_f1, &b_ph_ss_f1);
   fChain->SetBranchAddress("ph_ss_fracs1", &ph_ss_fracs1, &b_ph_ss_fracs1);
   fChain->SetBranchAddress("ph_ss_reta", &ph_ss_reta, &b_ph_ss_reta);
   fChain->SetBranchAddress("ph_ss_rhad", &ph_ss_rhad, &b_ph_ss_rhad);
   fChain->SetBranchAddress("ph_ss_rhad1", &ph_ss_rhad1, &b_ph_ss_rhad1);
   fChain->SetBranchAddress("ph_ss_rphi", &ph_ss_rphi, &b_ph_ss_rphi);
   fChain->SetBranchAddress("ph_ss_weta1", &ph_ss_weta1, &b_ph_ss_weta1);
   fChain->SetBranchAddress("ph_ss_weta2", &ph_ss_weta2, &b_ph_ss_weta2);
   fChain->SetBranchAddress("ph_ss_wtots1", &ph_ss_wtots1, &b_ph_ss_wtots1);
   fChain->SetBranchAddress("ph_drphb70_leading_pt", &ph_drphb70_leading_pt, &b_ph_drphb70_leading_pt);
   fChain->SetBranchAddress("ph_drphb77_leading_pt", &ph_drphb77_leading_pt, &b_ph_drphb77_leading_pt);
   fChain->SetBranchAddress("ph_drphb85_leading_pt", &ph_drphb85_leading_pt, &b_ph_drphb85_leading_pt);
   fChain->SetBranchAddress("ph_drphb70_subleading_pt", &ph_drphb70_subleading_pt, &b_ph_drphb70_subleading_pt);
   fChain->SetBranchAddress("ph_drphb77_subleading_pt", &ph_drphb77_subleading_pt, &b_ph_drphb77_subleading_pt);
   fChain->SetBranchAddress("ph_drphb85_subleading_pt", &ph_drphb85_subleading_pt, &b_ph_drphb85_subleading_pt);
   fChain->SetBranchAddress("ph_drphb70_leading_score", &ph_drphb70_leading_score, &b_ph_drphb70_leading_score);
   fChain->SetBranchAddress("ph_drphb77_leading_score", &ph_drphb77_leading_score, &b_ph_drphb77_leading_score);
   fChain->SetBranchAddress("ph_drphb85_leading_score", &ph_drphb85_leading_score, &b_ph_drphb85_leading_score);
   fChain->SetBranchAddress("ph_drphb70_subleading_score", &ph_drphb70_subleading_score, &b_ph_drphb70_subleading_score);
   fChain->SetBranchAddress("ph_drphb77_subleading_score", &ph_drphb77_subleading_score, &b_ph_drphb77_subleading_score);
   fChain->SetBranchAddress("ph_drphb85_subleading_score", &ph_drphb85_subleading_score, &b_ph_drphb85_subleading_score);
   fChain->SetBranchAddress("ph_drphb70_closest", &ph_drphb70_closest, &b_ph_drphb70_closest);
   fChain->SetBranchAddress("ph_drphb77_closest", &ph_drphb77_closest, &b_ph_drphb77_closest);
   fChain->SetBranchAddress("ph_drphb85_closest", &ph_drphb85_closest, &b_ph_drphb85_closest);
   fChain->SetBranchAddress("ph_mphb70_closest", &ph_mphb70_closest, &b_ph_mphb70_closest);
   fChain->SetBranchAddress("ph_mphb77_closest", &ph_mphb77_closest, &b_ph_mphb77_closest);
   fChain->SetBranchAddress("ph_mphb85_closest", &ph_mphb85_closest, &b_ph_mphb85_closest);
   fChain->SetBranchAddress("el_mlb70", &el_mlb70, &b_el_mlb70);
   fChain->SetBranchAddress("el_mlb77", &el_mlb77, &b_el_mlb77);
   fChain->SetBranchAddress("el_mlb85", &el_mlb85, &b_el_mlb85);
   fChain->SetBranchAddress("el_drlb70", &el_drlb70, &b_el_drlb70);
   fChain->SetBranchAddress("el_drlb77", &el_drlb77, &b_el_drlb77);
   fChain->SetBranchAddress("el_drlb85", &el_drlb85, &b_el_drlb85);
   fChain->SetBranchAddress("mu_mlb70", &mu_mlb70, &b_mu_mlb70);
   fChain->SetBranchAddress("mu_mlb77", &mu_mlb77, &b_mu_mlb77);
   fChain->SetBranchAddress("mu_mlb85", &mu_mlb85, &b_mu_mlb85);
   fChain->SetBranchAddress("mu_drlb70", &mu_drlb70, &b_mu_drlb70);
   fChain->SetBranchAddress("mu_drlb77", &mu_drlb77, &b_mu_drlb77);
   fChain->SetBranchAddress("mu_drlb85", &mu_drlb85, &b_mu_drlb85);
   fChain->SetBranchAddress("jet_mv2c10_ranked_index", &jet_mv2c10_ranked_index, &b_jet_mv2c10_ranked_index);
   fChain->SetBranchAddress("event_PV_z", &event_PV_z, &b_event_PV_z);
   fChain->SetBranchAddress("ph_z", &ph_z, &b_ph_z);
   fChain->SetBranchAddress("ph_dz", &ph_dz, &b_ph_dz);
   fChain->SetBranchAddress("ph_dz_ratio", &ph_dz_ratio, &b_ph_dz_ratio);
   fChain->SetBranchAddress("event_lumi", &event_lumi, &b_event_lumi);
   fChain->SetBranchAddress("event_norm", &event_norm, &b_event_norm);
   fChain->SetBranchAddress("ph_medium_index", &ph_medium_index, &b_ph_medium_index);
   Notify();
}

Bool_t nominal_tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void nominal_tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t nominal_tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef nominal_tree_cxx
