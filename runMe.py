# here we load the shared library created using the Makefile
from ROOT import gSystem, TH1, TStopwatch
from ROOT import gSystem
gSystem.Load('../build/libAnalysis.so')
TH1.AddDirectory(False)

# now we can create instance of the class EventLoop
from Samples import *
eventLoops = []
eventLoops += [ SampleData() ]
eventLoops += [ SampleTtyProdMCaMCd() ]
eventLoops += [ SampleTtyProdMCe() ]
eventLoops += [ SampleTtyDec() ]
eventLoops += [ SampleBkg() ]

# create algorithm
from Algorithms import *
for eventLoop in eventLoops:
  algs = []
  algs += [ AlgDefault() ]
  algs += [ AlgMu20() ]
  algs += [ AlgMu30() ]
  algs += [ AlgMu50() ]
  # add the algorithm into the event loop
  eventLoop.addAlgorithms( algs )

# execute parallel jobs
timer = TStopwatch()
from Jobs import Jobs
jobs = Jobs(eventLoops)
jobs.execute()
timer.Stop()
print "The processing took {} s".format(timer.RealTime())
